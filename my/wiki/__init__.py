from pyramid.config import Configurator


def includeme(config):
    config.add_route('top', '/')
    config.add_route('page', '/{pagename}',
                     factory=".resources.page_factory")
    config.scan(".views")


def main(global_conf, **settings):
    config = Configurator(settings=settings)
    config.include(".")
    config.include(".filemanager")
    return config.make_wsgi_app()
