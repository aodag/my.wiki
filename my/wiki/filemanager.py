import repoze.filesafe
from .interfaces import IFileManager


def includeme(config):
    reg = config.registry

    def register():
        reg.registerUtility(repoze.filesafe,
                            IFileManager)

    config.action("my.wiki.filemanager",
                  register)


def get_filemanager(request):
    return request.registry.getUtility(IFileManager)


def add_page(request, page):
    fm = get_filemanager(request)
    with fm.create_file(page.pagename, "w") as f:
        f.write(page.data)
