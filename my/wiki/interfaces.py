from zope.interface import Interface


class IFileManager(Interface):

    def create_file(filename, mode):
        pass

    def open_file(filename):
        pass

    def delete_file(filename):
        pass
