from pyramid.httpexceptions import HTTPNotFound
from repoze.filesafe import open_file
from .models import Page


def page_factory(request):
    pagename = request.matchdict['pagename']
    try:
        with open_file(pagename) as f:
            return Page(pagename, f.read())
    except IOError:
        return HTTPNotFound()
