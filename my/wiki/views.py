from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
from .models import Page
from .filemanager import add_page


@view_config(route_name="top")
def index(request):
    location = request.route_url("page", pagename="FrontPage")
    return HTTPFound(location=location)


@view_config(route_name="page", request_method="GET")
def page_view(context, request):
    return dict(page=context)


@view_config(route_name="page", request_method="POST")
def save_page_view(request):
    page = Page(request.matchdict['pagename'],
                request.POST['data'])
    add_page(request, page)
    return dict(page=page)
