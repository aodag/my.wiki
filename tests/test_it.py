import unittest
from pyramid import testing
from repoze.filesafe import testing as fs_testing
from testfixtures import compare, Comparison as C


class TestIndex(unittest.TestCase):

    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()

    def _callFUT(self, *args, **kwargs):
        from my.wiki.views import index
        return index(*args, **kwargs)

    def test_it(self):
        self.config.include('my.wiki')
        request = testing.DummyRequest()
        result = self._callFUT(request)

        compare(result.status, "302 Found")
        compare(result.location, "http://example.com/FrontPage")


class TestSavePageView(unittest.TestCase):

    def setUp(self):
        self.config = testing.setUp()
        self.datamanager = fs_testing.setup_dummy_data_manager()

    def tearDown(self):
        testing.tearDown()
        fs_testing.cleanup_dummy_data_manager()
        import transaction
        transaction.abort()

    def _callFUT(self, *args, **kwargs):
        from my.wiki.views import save_page_view
        return save_page_view(*args, **kwargs)

    def test_it(self):
        self.config.include('my.wiki')
        self.config.include('my.wiki.filemanager')
        request = testing.DummyRequest(
            matchdict={'pagename': 'TestPage'},
            POST={"data": "test page"})
        result = self._callFUT(request)

        with self.datamanager.open_file("TestPage") as f:
            data = f.read()
            compare(data, "test page")
