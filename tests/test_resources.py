import unittest
from repoze.filesafe import testing as fs_testing
from pyramid import testing
from testfixtures import compare


class TestPageFactory(unittest.TestCase):

    def setUp(self):
        self.datamanager = fs_testing.setup_dummy_data_manager()

    def tearDown(self):
        fs_testing.cleanup_dummy_data_manager()

    def _callFUT(self, *args, **kwargs):
        from my.wiki.resources import page_factory
        return page_factory(*args, **kwargs)

    def test_not_found(self):
        request = testing.DummyRequest(
            matchdict={"pagename": "no-page"})
        result = self._callFUT(request)

        compare(result.status, "404 Not Found")

    def test_found(self):
        with self.datamanager.create_file("one-page", "w") as f:
            f.write("OK")

        request = testing.DummyRequest(
            matchdict={"pagename": "one-page"})
        result = self._callFUT(request)

        compare(result.pagename, "one-page")
        compare(result.data, "OK")
